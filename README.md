# Lando MODX
## Introduction
This project spins up a fresh install of the latest build of MODX Revolution 2 in Lando. 
You will need to have [Lando](https://lando.dev) downloaded and installed in order for this project to run properly.

## Installation
From inside the base directory of this repository on your local machine, simply run:

```sh
$ lando start
$ lando modx:init
```

At this point, you can browse to http://modx.lndo.site/setup to set up MODX as you would in any other environment.  
**Important:** You must change the database name from "localhost" to "database" to connect the local appserver to the database server.
